/*globals initOccamVizCore*/
/*globals initOccamVizGraphs*/
/*globals initOccamVizGraphsBars*/
/*globals initOccamVizGraphsPie*/
var initOccamViz = function (context) {
  initOccamVizCore(context);

  // Add a similar line as above for each module that you have.  If you have a
  // module named "Awesome Module," it should live in the file
  // "src/occamViz.awesome-module.js" with a wrapper function named
  // "initAwesomeModule".
  initOccamVizGraphs(context);
  initOccamVizGraphsBars(context);
  initOccamVizGraphsPie(context);
  initOccamVizGraphsLines(context);

  return context.OccamViz;
};

if (typeof define === 'function' && define.amd) {
  // Expose OccamViz as an AMD module if it's loaded with RequireJS or
  // similar.
  define(function () {
    return initOccamViz({});
  });
} else {
  // Load OccamViz normally (creating a OccamViz global) if not using an AMD
  // loader.
  initOccamViz(this);
}
