/*global $, d3*/
function initOccamVizGraphsLines(context) {

  'use strict';

  var Graphs = context.OccamViz.Graphs;

  // Add to our repository of graphing options.
  Graphs._addGraph({
    name:        "lines",
    description: "A line charts."
  });

  /**
   * Draws a line graph in the specified div selector.
   * @param {String} options.selector The css selector pointing to the div to
   * append the graph.
   * @param {number} options.margin.left The left margin in pixels for the graph.
   * @param {number} options.margin.right The right margin in pixels for the graph.
   * @param {number} options.margin.top The top margin in pixels for the graph.
   * @param {number} options.margin.bottom The bottom margin in pixels for the graph.
   * @param {number} options.width The width in pixels of the graph.
   * @param {String} options.labels.x The text for the x axis (an array when there are multiple datapoints per x).
   * @param {String} options.labelx.y The text for the y axis.
   * @param {String} options.colors The color (described as CSS) to use for
   * the bars. This is an array when there are multiple datapoints per x.
   * @param {Array} options.data.groups The array of datapoints.
   */

  Graphs.prototype.lines = function(options) {
    options = options || {};

    options.data     = options.data     || {};
    options.width    = options.width    || 800;
    options.height   = options.height   || 400;
    options.colors   = options.colors   || [];
    options.margin   = options.margin   || {};
    options.labels   = options.labels   || {};
    options.domains  = options.domains  || {};

    // Default labels
    options.labels.x = options.labels.x || "foo";
    options.labels.y = options.labels.y || "bar";

    // Sanitize data
    options.data.groups = options.data.groups || [];

    // Set up domains
    var maxPoint = d3.max(options.data.groups, function( d ) {
      return d3.max(d.series[1]);
    });

    var minPoint = d3.min(options.data.groups, function( d ) {
      return d3.min(d.series[1]);
    });

    var minX = 0;

    var maxX = d3.max(options.data.groups, function( d ) {
      return d3.max(d.series, function(d){
        return d[0];
      });
    });

    var minGroups = 0;
    var maxGroups = options.data.groups.length;

    options.domains.x = options.domains.x || [minX, maxX];
    options.domains.y = options.domains.y || [minPoint, maxPoint];

    if (options.colors.length < maxGroups) {
      for (var i = options.colors.length; i < maxGroups; i++) {
        options.colors.push("hsl(" + ((360 / maxGroups) * i) + ", 60%, 60%)");
      }
    }

    var seriesSpace   = options.width / maxGroups;
    var seriesPadding = seriesSpace * 0.1;
    var seriesWidth   = seriesSpace - (seriesPadding * 2);

    var barSpace      = seriesWidth / options.data.groups.length;
    var barPadding    = barSpace * 0.1;
    var barWidth      = barSpace - (barPadding * 2);

    options.margin.top    = options.margin.top    || 10;
    options.margin.left   = options.margin.left   || 50;
    options.margin.right  = options.margin.right  || 10;
    options.margin.bottom = options.margin.bottom || 80 + (25 * maxGroups / 3);

    var chart_attr = {
      width:  options.width  + options.margin.left + options.margin.right,
      height: options.height + options.margin.top  + options.margin.bottom
    };

    var svg = d3.select(options.selector)
      .append("svg:svg")
      .attr("class", "chart")
      .attr('preserveAspectRatio', 'xMidYMid')
      .attr('viewBox', '0 0 '+chart_attr.width+' '+chart_attr.height)
      .attr(chart_attr);

    var chart = svg.append("svg:g")
                   .attr('transform', 'translate('+options.margin.left + ', ' + options.margin.top + ')');

    var x_series = d3.scale.ordinal()
                     .domain(options.labels.x)
                     .rangeBands([0, options.width]);

    var y = d3.scale.linear()
              .domain(options.domains.y.reverse())
              .rangeRound([0, options.height]);

    var x = d3.scale.linear()
                    .domain(options.domains.x)
                    .rangeRound([0, options.width]);

    var lineFunction = d3.svg.line()
      .x(function(d) { return x(d[0]); })
      .y(function(d) { return y(d[1]); })
      .interpolate("linear");

    // Lines
    var lines = chart.append('g')
                    .attr('class', 'lines');

    lines.selectAll('g')
      .data(options.data.groups)   // For every x axis entry
      .enter().append('svg:g')     // Create a group
        .attr('class', 'group')
        .selectAll('line')
        .data(options.data.groups) // For every group
        .enter().append("path")
            .datum(function(d){return d.series;})
            .attr("class", function(d, i) { return "line line-"+i; })
            .attr("d", lineFunction)
            .attr('data-group-index', function(d, i) { return i; })
            .style({
                "fill": "none",
                "stroke": function(d, i) {return options.colors[i];},
                "stroke-width": "2px"
            });
    
    
    // Points
    var points = chart.append('g')
                     .attr('class', 'points');
    
    var num = 0;
    var max = 0;
    points.selectAll('g')
      .data(options.data.groups)
      .enter().append('svg:g')
         .attr('class','group')
         .selectAll('circle')
         .data(function(d){max = d.series.length; return d.series;})
         .enter().append("svg:circle")
            .attr('data-group-index',function(d, i) {
                var myclass = num;
                if(i == max-1)
                    num = num+1;
                    return myclass; })
            .attr("stroke","black")
            .attr("fill", "black")
            .attr("r", 4)
            .attr("cx", function(d){ return x(d[0]) })
            .attr("cy", function(d){ return y(d[1]) })
            .style({
                "stroke":       "black",
                "stroke-width": "1px"
            });
        
        
    // Axis
    var xAxis = d3.svg.axis()
      .scale(x)
      .tickSize(6, 3, 1)
      .orient('bottom');

    chart.append('g')
      .attr('class', 'x axis')
      .attr('transform', 'translate(0, ' + options.height + ')')
      .call(xAxis)
      .append("text")
        .attr('class', 'x axis-label')
        .style({
          "text-anchor": "end"
        })
        .attr("transform", 'translate('+options.width/2+')')
        .attr("x", 7)
        .attr("dx", ".71em")
        .text(options.labels.x);

    var yAxis = d3.svg.axis()
      .scale(y)
      .tickSize(6, 3, 1)
      .orient('left');

    chart.append('g')
      .attr('class', 'y axis')
      .call(yAxis)
      .append("text")
      .attr('class', 'y axis-label')
      .style({
        "text-anchor": "end"
      })
      .attr("transform", "rotate(-90)")
      .attr("y", 6)
      .attr("dy", ".71em")
      .text(options.labels.y);

    var legend = chart.selectAll(".legend")
      .data(options.data.groups)
      .enter().append("g")
      .attr("class", "legend")
      .attr("transform", function(d, i) {
        var x = seriesSpace * (i % 3) + seriesPadding + barPadding;
        var y = options.height + options.margin.top + 40 + (25 * (i/3));
        return "translate(" + x + ", " + y + ")";
      });

    legend.append("text")
      .attr("dy", ".35em")
      .attr("transform", "translate(23,0)")
      .style("text-anchor", "start")
      .text(function(d) { return d.name; });

    legend.append("text")
      .attr("class", function(d, i) {
        return "legend-" + i;
      })
      .attr("dy", ".35em")
      .attr("transform", "translate(100,0)")
      .style({
        "text-anchor": "start",
      })
      .text("");
      
    legend.append("rect")
      .attr("class", function(d, i) {
        return "legend-" + i;
      })
      .attr("transform", "translate(0,-9)")
            .attr('data-group-index', function(d, i) { return i; })
      .attr("width", 18)
      .attr("height", 18)
      .style("fill", function(d, i){
        return options.colors[i];
      });

    $(options.selector + " svg g.legend rect")
      .on('mouseenter', function(e) {
        $(this).css({
          "stroke":       "black",
          "stroke-width": "3px"
        });

        d3.select(options.selector + " svg g.lines g.group path.line-"+ $(this).data("group-index"))
          .style({
            "stroke-width": "5px"
          });
        })
      .on('mouseout', function(e) {
        $(this).css({
          "stroke":       "",
          "stroke-width": ""
        });

        d3.select(options.selector + " svg g.lines g.group path.line-"+ $(this).data("group-index"))
          .style({
            "stroke-width": ""
          });
      });

    $(options.selector + " svg g.lines g.group path.line")
      .on('mouseenter', function(e) {
        $(this).css({
          "stroke-width": "5px"
        });
        d3.select(options.selector + " rect.legend-" + $(this).data("group-index"))
          .style({
            "stroke":       "black",
            "stroke-width": "3px"
          });
        })
      .on('mouseout', function(e) {
        $(this).css({
          "stroke-width": ""
        });

        d3.select(options.selector + " rect.legend-" + $(this).data("group-index"))
          .style({
            "stroke":       "",
            "stroke-width": ""
          });
      });
      
      
    //Highlight points when close to mouse and display values to legend

     svg.on('mousemove',function(){
       var mousex = d3.mouse(this)[0];
       var n = 0;
       d3.select(options.selector + " svg g.points " ).selectAll("g.group").each(function (d,i){
         n = d.series.length;
       });
            
       d3.select(options.selector + " svg g.points " ).selectAll("g.group").selectAll("circle").each(function (d,i){	
         var curx = parseInt(d3.select(this).attr("cx"));		
         curx = curx+options.margin.left;

         var t = (options.width+60)/n;
         if(curx >= mousex - t/2 && curx <= mousex + t/2){
           d3.select(this)
             .style({
                "stroke":       "black",
                "stroke-width": "5px"
              });
           d3.select(options.selector + " text.legend-" + $(this).data("group-index"))
             .text(":  "+d[1]); 
           }
           else{
             d3.select(this)
               .style({
                 "stroke":       "black",
                 "stroke-width": "1px"
               });
             //d3.select(options.selector + " text.legend-" + $(this).data("group-index"))
             //  .text("");			
          }
       })
    });
  };
}