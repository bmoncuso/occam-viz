/*global $, d3*/
function initOccamVizGraphsPie(context) {
  'use strict';

  var uuid = 0;
  var Graphs = context.OccamViz.Graphs;

  // Add to our repository of graphing options.
  Graphs._addGraph({
    name:        "pie",
    description: "A pie chart."
  });

  /**
   * Draws a pie chart in the specified div selector.
   * @param {String} options.selector The css selector pointing to the div to
   * append the graph.
   */
  Graphs.prototype.pie = function(options) {
    options = options || {};

    options.data     = options.data     || {};
    options.width    = options.width    || 800;
    options.height   = options.height   || 400;
    options.colors   = options.colors   || [];
    options.margin   = options.margin   || {};
    options.labels   = options.labels   || {};
    options.domains  = options.domains  || {};

    // Default labels
    options.labels.x = options.labels.x || "foo";
    options.labels.y = options.labels.y || "bar";

    // Sanitize data
    options.data.groups = options.data.groups || [];

    options.margin.top    = options.margin.top    || 50;
    options.margin.left   = options.margin.left   || 50;
    options.margin.right  = options.margin.right  || 50;
    options.margin.bottom = options.margin.bottom || 50;

    var chart_attr = {
      width:  options.width  + options.margin.left + options.margin.right,
      height: options.height + options.margin.top  + options.margin.bottom
    };

    var minGroups = 0;
    var maxGroups = options.data.groups.length;

    if (options.colors.length < maxGroups) {
      for (var i = options.colors.length; i < maxGroups; i++) {
        options.colors.push("hsl(" + ((360 / maxGroups) * i) + ", 60%, 60%)");
      }
    }

    // Pie charts
    var radius = 100;
    var pie_layout = d3.layout.pie()
                              .sort(null);

    var svg = d3.select(options.selector)
      .append("svg:svg")
      .attr("class", "pie chart")
      .attr('preserveAspectRatio', 'xMidYMid')
      .attr('viewBox', '0 0 ' + chart_attr.width + ' ' + chart_attr.height);

    var defs = svg.append('defs');

    var chart_size = d3.min([options.width, options.height]);

    var arc = d3.svg.arc()
                    .innerRadius(function(d, i, j) { return j * (chart_size / 2 / options.labels.x.length); })
                    .outerRadius(function(d, i, j) { return (j+1) * (chart_size / 2 / options.labels.x.length); });

    var chart = svg.append("svg:g")
                   .attr('transform', 'translate(' + options.margin.left + ", "
                         + options.margin.top + ")");

    var pies = chart.append('g')
                    .attr('class', 'pies')
                    .attr('transform', 'translate(' + options.width / 2 + ', ' + options.height / 2 + ')');

    pies.selectAll('g.pie')
        .data(options.labels.x)
        .enter().append('svg:g')
          .attr('class', function(d,i) { return 'group pie pie-' + i; })
          .selectAll('path.slice')
          .data(function(d, i) {
            return pie_layout(
              options.data.groups.map(function(e) {
                return e.series[i];
              })
            );
          })
          .enter().append('path')
            .attr('class', function(d, i) { return 'slice slice-' + i; })
            .attr('d', arc)
            .attr('data-group-index', function(d, i) { return i; })
            .style('fill', function(d, i) { return options.colors[i]});

    $(options.selector + " svg g.pies g.pie path.slice")
      .on('mouseenter', function(e) {
        chart.selectAll("g.pies g.pie path.slice")
          .style({
            "opacity":      "0.6",
          });

        chart.selectAll("g.pies g.pie path.slice.slice-" + $(this).data("group-index"))
          .style({
            "opacity":      "1.0",
          });
      })
      .on('mouseout', function(e) {
        chart.selectAll("g.pies g.pie path.slice")
          .style({
            "opacity":      "1.0",
          });
      });

    var labels = chart.append('svg:g')
                      .attr('class', 'series labels');

    // Labels for stacked pie chart
    if (options.labels.x.length > 1) {
      labels.selectAll('line.label')
        .data(options.labels.x)
        .enter().append("svg:line")
          .attr('x1', options.width / 2)
          .attr('y1', function(d, i) {
            return (i+0.5) * (chart_size / 2 / options.labels.x.length);
          })
          .attr('x2', options.width)
          .attr('y2', function(d, i) {
            return (i+0.5) * (chart_size / 2 / options.labels.x.length);
          })
          .attr('class', 'label')
          .attr('stroke', 'rgba(255, 255, 255, 0.7)');

      labels.selectAll('rect.label')
        .data(options.labels.x)
        .enter().append("svg:rect")
          .attr('fill', 'rgba(255, 255, 255, 0.7)')
          .attr('class', 'label')
          .attr('data-series-index', function(d,i) { return options.labels.x.length-1-i; })
          .attr('x', options.width - ((options.width - chart_size - 20) / 2))
          .attr('y', function(d, i) {
            return (i+0.5) * (chart_size / 2 / options.labels.x.length) - 30;
          })
          .attr('height', 30)
          .attr('width', (options.width - chart_size - 20) / 2);

      var label_width = ((options.width - chart_size - 20) / 2);

      labels.selectAll('text.label')
        .data(options.labels.x)
        .enter().append("svg:text")
          .attr('x', options.width - label_width/2)
          .attr('y', function(d, i) {
            return (options.labels.x.length-1-i+0.5) * (chart_size / 2 / options.labels.x.length) - 15;
          })
          .attr('text-anchor', 'middle')
          .attr('data-series-index', function(d,i) { return i; })
          .attr('class', 'label')
          .style('alignment-baseline', 'central')
          .style('dominant-baseline', 'central')
          .text(function(d,i) { return d; });

      $(options.selector + " svg g.series.labels .label")
        .on('mouseenter', function(e) {
          chart.selectAll("g.pies g.pie")
            .style({
              "opacity":      "0.6",
            });

          chart.selectAll("g.pies g.pie.pie-" + $(this).data("series-index"))
            .style({
              "opacity":      "",
            });
        })
        .on('mouseout', function(e) {
          chart.selectAll("g.pies g.pie")
            .style({
              "opacity":      "",
            });
        });
    }

    var group_labels = chart.append('svg:g')
                            .attr('class', 'group labels')
                            .attr('transform', 'translate(' + options.width / 2 + ', ' + options.height / 2 + ')');

    var groups = options.data.groups.map(function(group) {
      return group.name;
    });

    // Add group labels
    // Put them by default at locations radiating out from graph
    //   from the center of the arcs that correspond
    // Fit them inside the chart, do not allow them to overlap circle
    //   or pie chart legend or group legend (Initially empty)
    // If lines from center of arc to text go through circle or pie
    //   chart legend, put those labels inside group legend
    // add group legend, repeat process until fit
    // Add lines

    var group_arc = d3.svg.arc()
                          .innerRadius(chart_size / 2 + 100)
                          .outerRadius(chart_size / 2 + 100);

    var edge_arc = d3.svg.arc()
                         .innerRadius(chart_size / 2)
                         .outerRadius(chart_size / 2);

    // Assign default label locations around circle
    group_labels.selectAll('text.slice')
      .data(pie_layout(
        options.data.groups.map(function(e) {
          return e.series[e.series.length-1];
        })
      ))
      .enter().append('text')
        .attr('class', 'slice')
        .attr('x', function(d) {
          return group_arc.centroid(d)[0];
        })
        .attr('y', function(d) {
          return group_arc.centroid(d)[1];
        })
        .attr('dy', -5)
        .attr('dx', function(d) {
          if ((d.endAngle + d.startAngle)/2 > Math.PI) {
            return -5;
          }
          else {
            return 5;
          }
        })
        .attr('text-anchor', function(d) {
          if ((d.endAngle + d.startAngle)/2 > Math.PI) {
            return "end";
          }
          else {
            return "start";
          }
        })
        .text(function(d, i) {
          return options.data.groups[i].name;
        })
        .style('fill', "#ddd");

    // Move labels that are not in the graph
    group_labels.selectAll('text').each(function() {
      var old_y = d3.select(this).attr('y');
      if (old_y < -(chart_attr.height/2)+30) {
        old_y = -(chart_attr.height/2)+30;
      }
      if (old_y > (chart_attr.height/2)-30) {
        old_y = (chart_attr.height/2)-30;
      }
      d3.select(this).attr('y', old_y);
    });

    // Move labels that are inside pie legend
    if (options.labels.x.length > 1) {
      var pie_legend = {
        x1: 0,
        y1: -0.5 * (chart_size / 2 / options.labels.x.length),
        x2: chart_attr.width / 2,
        y2: -(options.labels.x.length-0.5) * (chart_size / 2 / options.labels.x.length) - 30
      };

      group_labels.selectAll('text').each(function() {
        var old_y = d3.select(this).attr('y');
        var old_x = d3.select(this).attr('x');
        if (old_x > pie_legend.x1 &&
            old_y > pie_legend.y2 && old_y < pie_legend.y1) {
          old_y = pie_legend.y1 + 30;
        }
        d3.select(this).attr('y', old_y);
      });
    }

    // Add lines
    group_labels.selectAll('line')
      .data(pie_layout(
        options.data.groups.map(function(e) {
          return e.series[e.series.length-1];
        })
      ))
      .enter().append('line')
        .attr('class', 'slice')
        .attr('x1', function(d) {
          return edge_arc.centroid(d)[0];
        })
        .attr('y1', function(d) {
          return edge_arc.centroid(d)[1];
        })
        .attr('x2', function(d,i) {
          return group_labels.select('text:nth-child('+(i+1)+')').attr('x');
        })
        .attr('y2', function(d,i) {
          return group_labels.select('text:nth-child('+(i+1)+')').attr('y');
        })
        .style('stroke', function(d, i) {
          return options.colors[i];
        });

    group_labels.selectAll('line.underline')
      .data(options.data.groups)
      .enter().append('line')
        .attr('class', 'slice underline')
        .attr('x1', function(d,i) {
          var label = group_labels.select('text:nth-child('+(i+1)+')');
          if (label.attr('text-anchor') === 'end') {
            return label.node().getBBox().x;
          }
          else {
            return label.node().getBBox().x + label.node().getBBox().width;
          }
        })
        .attr('y1', function(d,i) {
          return group_labels.select('text:nth-child('+(i+1)+')').attr('y');
        })
        .attr('x2', function(d,i) {
          return group_labels.select('text:nth-child('+(i+1)+')').attr('x');
        })
        .attr('y2', function(d,i) {
          return group_labels.select('text:nth-child('+(i+1)+')').attr('y');
        })
        .style('stroke', function(d, i) {
          return options.colors[i];
        });

    uuid += 1;
  };
}
