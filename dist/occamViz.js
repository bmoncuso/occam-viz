/*! lib-occamViz - v0.1.0 - 2014-04-21 - OCCAM */
;(function (global) {

/*jslint browser: true*/
/*global $*/

// Compiler directive for UglifyJS.  See occamViz.const.js for more info.
if (typeof DEBUG === 'undefined') {
  DEBUG = true;
}

// LIBRARY-GLOBAL CONSTANTS
//
// These constants are exposed to all library modules.

// GLOBAL is a reference to the global Object.
var Fn = Function, GLOBAL = new Fn('return this')();

// LIBRARY-GLOBAL METHODS
//
// The methods here are exposed to all library modules.  Because all of the
// source files are wrapped within a closure at build time, they are not
// exposed globally in the distributable binaries.

/**
 * A no-op function.  Useful for passing around as a default callback.
 */
function noop () { }

/**
 * Init wrapper for the core module.
 * @param {Object} The Object that the library gets attached to in
 * occamViz.init.js. If the library was not loaded with an AMD loader such as
 * require.js, this is the global Object.
 */
function initOccamVizCore (context) {
  'use strict';

  // PRIVATE MODULE CONSTANTS
  //

  // An example of a CONSTANT variable;
  var CORE_CONSTANT = true;

  // PRIVATE MODULE METHODS
  //
  // These do not get attached to a prototype. They are private utility
  // functions.

  /**
   * This is the constructor for the OccamViz Object.
   * Note that the constructor is also being
   * attached to the context that the library was loaded in.
   * @param {Object} opt_config Contains any properties that should be used to
   * configure this instance of the library.
   * @constructor
   */
  var OccamViz = context.OccamViz = function (opt_config) {
    opt_config = opt_config || {};

    opt_config.domain = opt_config.domain || "";

    // Read only (led by underscores)
    this._domain = opt_config.domain;

    // Accessible
    this.graphs = new OccamViz.Graphs();

    return this;
  };

  // LIBRARY PROTOTYPE METHODS
  //
  // These methods define the public API.

  /**
   * Returns the set domain. An empty string means it is polling the source
   * domain for the script.
   * @return {string}
   */
  OccamViz.prototype.getDomain = function() {
    return this._domain;
  };

  /**
   * Callback for rendering results.
   *
   * @callback resultsCallback
   * @param {Object} results An object defining the results.
   */

  /**
   * Returns the results for the given experiment.
   * @param {number} experiment_id The id of the experiment to query.
   * @param {resultsCallback} success A callback that fires when the results
   * are available.
   * @return {OccamViz}
   */
  OccamViz.prototype.getResults = function(experiment_id, success) {
    var url = this._domain + "/experiments/" + experiment_id + "/results";
    $.getJSON(url, function(data) {
      success(data);
    });

    return this;
  };

  // DEBUG CODE
  //
  // With compiler directives, you can wrap code in a conditional check to
  // ensure that it does not get included in the compiled binaries.  This is
  // useful for exposing certain properties and methods that are needed during
  // development and testing, but should be private in the compiled binaries.
  if (DEBUG) {
  }
}

/*global $, d3*/
function initOccamVizGraphsBars(context) {
  'use strict';

  var Graphs = context.OccamViz.Graphs;

  // Add to our repository of graphing options.
  Graphs._addGraph({
    name:        "bars",
    description: "A bar charts."
  });

  /**
   * Draws a bar graph in the specified div selector.
   * @param {String} options.selector The css selector pointing to the div to
   * append the graph.
   * @param {number} options.margin.left The left margin in pixels for the graph.
   * @param {number} options.margin.right The right margin in pixels for the graph.
   * @param {number} options.margin.top The top margin in pixels for the graph.
   * @param {number} options.margin.bottom The bottom margin in pixels for the graph.
   * @param {number} options.width The width in pixels of the graph.
   * @param {String} options.labels.x The text for the x axis (an array when there are multiple datapoints per x).
   * @param {String} options.labelx.y The text for the y axis.
   * @param {String} options.colors The color (described as CSS) to use for
   * the bars. This is an array when there are multiple datapoints per x.
   * @param {Array} options.data.groups The array of datapoints.
   */

  Graphs.prototype.bars = function(options) {
    options = options || {};

    options.data     = options.data     || {};
    options.width    = options.width    || 800;
    options.height   = options.height   || 400;
    options.colors   = options.colors   || [];
    options.margin   = options.margin   || {};
    options.labels   = options.labels   || {};
    options.domains  = options.domains  || {};

    // Default labels
    options.labels.x = options.labels.x || "foo";
    options.labels.y = options.labels.y || "bar";

    // Sanitize data
    options.data.groups = options.data.groups || [];

    // Set up domains
    var maxPoint = d3.max(options.data.groups, function( d ) {
      return d3.max(d.series);
    });

    var minPoint = d3.min(options.data.groups, function( d ) {
      return d3.min(d.series);
    });

    var minX = 0;
    var maxX = d3.max([options.labels.x.length - 1,
                       d3.max(options.data.groups, function(d) {
                          return d.series.length;
                       })
                      ]);

    var minGroups = 0;
    var maxGroups = options.data.groups.length;

    options.domains.x = options.domains.x || [minX, maxX];
    options.domains.y = options.domains.y || [minPoint, maxPoint];

    if (options.colors.length < maxGroups) {
      for (var i = options.colors.length; i < maxGroups; i++) {
        options.colors.push("hsl(" + ((360 / maxGroups) * i) + ", 60%, 60%)");
      }
    }

    var seriesSpace   = options.width / options.labels.x.length;
    var seriesPadding = seriesSpace * 0.1;
    var seriesWidth   = seriesSpace - (seriesPadding * 2);

    var barSpace      = seriesWidth / options.data.groups.length;
    var barPadding    = barSpace * 0.1;
    var barWidth      = barSpace - (barPadding * 2);

    options.margin.top    = options.margin.top    || 10;
    options.margin.left   = options.margin.left   || 50;
    options.margin.right  = options.margin.right  || 10;
    options.margin.bottom = options.margin.bottom || 80 + (25 * maxGroups / 3);

    var chart_attr = {
      width:  options.width  + options.margin.left + options.margin.right,
      height: options.height + options.margin.top  + options.margin.bottom
    };

    var svg = d3.select(options.selector)
      .append("svg:svg")
      .attr("class", "chart")
      .attr('preserveAspectRatio', 'xMidYMid')
      .attr('viewBox', '0 0 '+chart_attr.width+' '+chart_attr.height)
      .attr(chart_attr);

    var chart = svg.append("svg:g")
                   .attr('transform', 'translate('+options.margin.left + ', ' + options.margin.top + ')');

    var x_series = d3.scale.ordinal()
                     .domain(options.labels.x)
                     .rangeBands([0, options.width]);

    var y = d3.scale.linear()
              .domain(options.domains.y.reverse())
              .rangeRound([0, options.height]);

    // Bars
    var bars = chart.append('g')
                    .attr('class', 'bars');

    bars.selectAll('g')
      .data(options.labels.x)      // For every x axis entry
      .enter().append('svg:g')     // Create a group
        .attr('class', 'group')
        .attr('transform', function(d, i) {
          return 'translate(' + (seriesSpace * i) + ', 0)';
        })
        .selectAll('rect')
        .data(options.data.groups) // For every group
        .enter().append('rect')    // Create a rectangle
          .attr('x', function(d, i) {
            return seriesPadding + barSpace * i + barPadding;
          })
          .attr('y', function(d, i, j) {
            // i is the index of the group we are in
            // j is the index of our x-axis position
            return y(d.series[j]) + 0.5;
          })
          .attr('data-group-index', function(d, i) { return i; })
          .attr('width', barWidth)
          .attr('height', function(d, i, j) {
            return options.height - y(d.series[j]);
          })
          .style({
            fill: function(d, i) {
              return options.colors[i];
            }
          });

  $(options.selector + " svg g.bars g.group rect")
    .on('mouseenter', function(e) {
      d3.select(options.selector + " rect.legend-" + $(this).data("group-index"))
        .style({
          "stroke":       "black",
          "stroke-width": "3px"
        });
    })
    .on('mouseout', function(e) {
      d3.select(options.selector + " rect.legend-" + $(this).data("group-index"))
        .style({
          "stroke":       "",
          "stroke-width": ""
        });
    });

    // Axis
    var xAxis = d3.svg.axis()
      .scale(x_series)
      .tickSize(6, 3, 1)
      .tickValues(options.labels.x);

    chart.append('g')
      .attr('class', 'x axis')
      .attr('transform', 'translate(0, ' + options.height + ')')
      .call(xAxis);

    var yAxis = d3.svg.axis()
      .scale(y)
      .tickSize(6, 3, 1)
      .orient('left');

    chart.append('g')
      .attr('class', 'y axis')
      .call(yAxis)
      .append("text")
      .attr('class', 'y axis-label')
      .style({
        "text-anchor": "end"
      })
      .attr("transform", "rotate(-90)")
      .attr("y", 6)
      .attr("dy", ".71em")
      .text(options.labels.y);

    var legend = chart.selectAll(".legend")
      .data(options.data.groups)
      .enter().append("g")
      .attr("class", "legend")
      .attr("transform", function(d, i) {
        var x = seriesSpace * (i % 3) + seriesPadding + barPadding;
        var y = options.height + options.margin.top + 40 + (25 * (i/3));
        return "translate(" + x + ", " + y + ")";
      });

    legend.append("text")
      .attr("dy", ".35em")
      .attr("transform", "translate(23,0)")
      .style("text-anchor", "start")
      .text(function(d) { return d.name; });

    legend.append("rect")
      .attr("class", function(d, i) {
        return "legend-" + i;
      })
      .attr("transform", "translate(0,-9)")
      .attr("width", 18)
      .attr("height", 18)
      .style("fill", function(d, i){
        return options.colors[i];
      });
  };
}

function initOccamVizGraphs(context) {
  'use strict';

  var OccamViz = context.OccamViz;

  /**
   * The graphs submodule constructor.
   * @param {Object} opt_config Contains any properties that should be used to
   * configure this instance of the library.
   * @constructor
   */
  var submodule = OccamViz.Graphs = function(opt_config) {
    opt_config = opt_config || {};
  };

  submodule._graphTypes = [];

  /**
   * Returns the graph types available.
   * @returns {Array} An array of objects describing each graph type available.
   * The entry contains a "name", "description" which has a longer description
   * of the graph type.
   */
  submodule.getGraphs = function() {
    return submodule._graphTypes;
  };

  // LIBRARY PROTOTYPE EXTENSIONS

  // PRIVATE METHODS

  /**
   * Adds the given graph type to the list of graphs available. This list
   * is returned with OccamViz.Graphs.getGraphs().
   * @param {String} graphInfo.name The name of the graphing option.
   * @param {String} graphInfo.description A long description of the graphing
   * option.
   */
  submodule._addGraph = function(graphInfo) {
    OccamViz.Graphs._graphTypes.push(graphInfo);
  };
}

/*global $, d3*/
function initOccamVizGraphsLines(context) {

  'use strict';

  var Graphs = context.OccamViz.Graphs;

  // Add to our repository of graphing options.
  Graphs._addGraph({
    name:        "lines",
    description: "A line charts."
  });

  /**
   * Draws a line graph in the specified div selector.
   * @param {String} options.selector The css selector pointing to the div to
   * append the graph.
   * @param {number} options.margin.left The left margin in pixels for the graph.
   * @param {number} options.margin.right The right margin in pixels for the graph.
   * @param {number} options.margin.top The top margin in pixels for the graph.
   * @param {number} options.margin.bottom The bottom margin in pixels for the graph.
   * @param {number} options.width The width in pixels of the graph.
   * @param {String} options.labels.x The text for the x axis (an array when there are multiple datapoints per x).
   * @param {String} options.labelx.y The text for the y axis.
   * @param {String} options.colors The color (described as CSS) to use for
   * the bars. This is an array when there are multiple datapoints per x.
   * @param {Array} options.data.groups The array of datapoints.
   */

  Graphs.prototype.lines = function(options) {
    options = options || {};

    options.data     = options.data     || {};
    options.width    = options.width    || 800;
    options.height   = options.height   || 400;
    options.colors   = options.colors   || [];
    options.margin   = options.margin   || {};
    options.labels   = options.labels   || {};
    options.domains  = options.domains  || {};

    // Default labels
    options.labels.x = options.labels.x || "foo";
    options.labels.y = options.labels.y || "bar";

    // Sanitize data
    options.data.groups = options.data.groups || [];

    // Set up domains
    var maxPoint = d3.max(options.data.groups, function( d ) {
      return d3.max(d.series[1]);
    });

    var minPoint = d3.min(options.data.groups, function( d ) {
      return d3.min(d.series[1]);
    });

    var minX = 0;

    var maxX = d3.max(options.data.groups, function( d ) {
      return d3.max(d.series, function(d){
        return d[0];
      });
    });

    var minGroups = 0;
    var maxGroups = options.data.groups.length;

    options.domains.x = options.domains.x || [minX, maxX];
    options.domains.y = options.domains.y || [minPoint, maxPoint];

    if (options.colors.length < maxGroups) {
      for (var i = options.colors.length; i < maxGroups; i++) {
        options.colors.push("hsl(" + ((360 / maxGroups) * i) + ", 60%, 60%)");
      }
    }

    var seriesSpace   = options.width / maxGroups;
    var seriesPadding = seriesSpace * 0.1;
    var seriesWidth   = seriesSpace - (seriesPadding * 2);

    var barSpace      = seriesWidth / options.data.groups.length;
    var barPadding    = barSpace * 0.1;
    var barWidth      = barSpace - (barPadding * 2);

    options.margin.top    = options.margin.top    || 10;
    options.margin.left   = options.margin.left   || 50;
    options.margin.right  = options.margin.right  || 10;
    options.margin.bottom = options.margin.bottom || 80 + (25 * maxGroups / 3);

    var chart_attr = {
      width:  options.width  + options.margin.left + options.margin.right,
      height: options.height + options.margin.top  + options.margin.bottom
    };

    var svg = d3.select(options.selector)
      .append("svg:svg")
      .attr("class", "chart")
      .attr('preserveAspectRatio', 'xMidYMid')
      .attr('viewBox', '0 0 '+chart_attr.width+' '+chart_attr.height)
      .attr(chart_attr);

    var chart = svg.append("svg:g")
                   .attr('transform', 'translate('+options.margin.left + ', ' + options.margin.top + ')');

    var x_series = d3.scale.ordinal()
                     .domain(options.labels.x)
                     .rangeBands([0, options.width]);

    var y = d3.scale.linear()
              .domain(options.domains.y.reverse())
              .rangeRound([0, options.height]);

    var x = d3.scale.linear()
                    .domain(options.domains.x)
                    .rangeRound([0, options.width]);

    var lineFunction = d3.svg.line()
      .x(function(d) { return x(d[0]); })
      .y(function(d) { return y(d[1]); })
      .interpolate("linear");

    // Lines
    var lines = chart.append('g')
                    .attr('class', 'lines');

    lines.selectAll('g')
      .data(options.data.groups)   // For every x axis entry
      .enter().append('svg:g')     // Create a group
        .attr('class', 'group')
        .selectAll('line')
        .data(options.data.groups) // For every group
        .enter().append("path")
            .datum(function(d){return d.series;})
            .attr("class", function(d, i) { return "line line-"+i; })
            .attr("d", lineFunction)
            .attr('data-group-index', function(d, i) { return i; })
            .style({
                "fill": "none",
                "stroke": function(d, i) {return options.colors[i];},
                "stroke-width": "2px"
            });
    
    
    // Points
    var points = chart.append('g')
                     .attr('class', 'points');
    
    var num = 0;
    var max = 0;
    points.selectAll('g')
      .data(options.data.groups)
      .enter().append('svg:g')
         .attr('class','group')
         .selectAll('circle')
         .data(function(d){max = d.series.length; return d.series;})
         .enter().append("svg:circle")
            .attr('data-group-index',function(d, i) {
                var myclass = num;
                if(i == max-1)
                    num = num+1;
                    return myclass; })
            .attr("stroke","black")
            .attr("fill", "black")
            .attr("r", 4)
            .attr("cx", function(d){ return x(d[0]) })
            .attr("cy", function(d){ return y(d[1]) })
            .style({
                "stroke":       "black",
                "stroke-width": "1px"
            });
        
        
    // Axis
    var xAxis = d3.svg.axis()
      .scale(x)
      .tickSize(6, 3, 1)
      .orient('bottom');

    chart.append('g')
      .attr('class', 'x axis')
      .attr('transform', 'translate(0, ' + options.height + ')')
      .call(xAxis)
      .append("text")
        .attr('class', 'x axis-label')
        .style({
          "text-anchor": "end"
        })
        .attr("transform", 'translate('+options.width/2+')')
        .attr("x", 7)
        .attr("dx", ".71em")
        .text(options.labels.x);

    var yAxis = d3.svg.axis()
      .scale(y)
      .tickSize(6, 3, 1)
      .orient('left');

    chart.append('g')
      .attr('class', 'y axis')
      .call(yAxis)
      .append("text")
      .attr('class', 'y axis-label')
      .style({
        "text-anchor": "end"
      })
      .attr("transform", "rotate(-90)")
      .attr("y", 6)
      .attr("dy", ".71em")
      .text(options.labels.y);

    var legend = chart.selectAll(".legend")
      .data(options.data.groups)
      .enter().append("g")
      .attr("class", "legend")
      .attr("transform", function(d, i) {
        var x = seriesSpace * (i % 3) + seriesPadding + barPadding;
        var y = options.height + options.margin.top + 40 + (25 * (i/3));
        return "translate(" + x + ", " + y + ")";
      });

    legend.append("text")
      .attr("dy", ".35em")
      .attr("transform", "translate(23,0)")
      .style("text-anchor", "start")
      .text(function(d) { return d.name; });

    legend.append("text")
      .attr("class", function(d, i) {
        return "legend-" + i;
      })
      .attr("dy", ".35em")
      .attr("transform", "translate(100,0)")
      .style({
        "text-anchor": "start",
      })
      .text("");
      
    legend.append("rect")
      .attr("class", function(d, i) {
        return "legend-" + i;
      })
      .attr("transform", "translate(0,-9)")
            .attr('data-group-index', function(d, i) { return i; })
      .attr("width", 18)
      .attr("height", 18)
      .style("fill", function(d, i){
        return options.colors[i];
      });

    $(options.selector + " svg g.legend rect")
      .on('mouseenter', function(e) {
        $(this).css({
          "stroke":       "black",
          "stroke-width": "3px"
        });

        d3.select(options.selector + " svg g.lines g.group path.line-"+ $(this).data("group-index"))
          .style({
            "stroke-width": "5px"
          });
        })
      .on('mouseout', function(e) {
        $(this).css({
          "stroke":       "",
          "stroke-width": ""
        });

        d3.select(options.selector + " svg g.lines g.group path.line-"+ $(this).data("group-index"))
          .style({
            "stroke-width": ""
          });
      });

    $(options.selector + " svg g.lines g.group path.line")
      .on('mouseenter', function(e) {
        $(this).css({
          "stroke-width": "5px"
        });
        d3.select(options.selector + " rect.legend-" + $(this).data("group-index"))
          .style({
            "stroke":       "black",
            "stroke-width": "3px"
          });
        })
      .on('mouseout', function(e) {
        $(this).css({
          "stroke-width": ""
        });

        d3.select(options.selector + " rect.legend-" + $(this).data("group-index"))
          .style({
            "stroke":       "",
            "stroke-width": ""
          });
      });
      
      
    //Highlight points when close to mouse and display values to legend

     svg.on('mousemove',function(){
       var mousex = d3.mouse(this)[0];
       var n = 0;
       d3.select(options.selector + " svg g.points " ).selectAll("g.group").each(function (d,i){
         n = d.series.length;
       });
            
       d3.select(options.selector + " svg g.points " ).selectAll("g.group").selectAll("circle").each(function (d,i){	
         var curx = parseInt(d3.select(this).attr("cx"));		
         curx = curx+options.margin.left;

         var t = (options.width+60)/n;
         if(curx >= mousex - t/2 && curx <= mousex + t/2){
           d3.select(this)
             .style({
                "stroke":       "black",
                "stroke-width": "5px"
              });
           d3.select(options.selector + " text.legend-" + $(this).data("group-index"))
             .text(":  "+d[1]); 
           }
           else{
             d3.select(this)
               .style({
                 "stroke":       "black",
                 "stroke-width": "1px"
               });
             //d3.select(options.selector + " text.legend-" + $(this).data("group-index"))
             //  .text("");			
          }
       })
    });
  };
}
/*global $, d3*/
function initOccamVizGraphsPie(context) {
  'use strict';

  var uuid = 0;
  var Graphs = context.OccamViz.Graphs;

  // Add to our repository of graphing options.
  Graphs._addGraph({
    name:        "pie",
    description: "A pie chart."
  });

  /**
   * Draws a pie chart in the specified div selector.
   * @param {String} options.selector The css selector pointing to the div to
   * append the graph.
   */
  Graphs.prototype.pie = function(options) {
    options = options || {};

    options.data     = options.data     || {};
    options.width    = options.width    || 800;
    options.height   = options.height   || 400;
    options.colors   = options.colors   || [];
    options.margin   = options.margin   || {};
    options.labels   = options.labels   || {};
    options.domains  = options.domains  || {};

    // Default labels
    options.labels.x = options.labels.x || "foo";
    options.labels.y = options.labels.y || "bar";

    // Sanitize data
    options.data.groups = options.data.groups || [];

    options.margin.top    = options.margin.top    || 50;
    options.margin.left   = options.margin.left   || 50;
    options.margin.right  = options.margin.right  || 50;
    options.margin.bottom = options.margin.bottom || 50;

    var chart_attr = {
      width:  options.width  + options.margin.left + options.margin.right,
      height: options.height + options.margin.top  + options.margin.bottom
    };

    var minGroups = 0;
    var maxGroups = options.data.groups.length;

    if (options.colors.length < maxGroups) {
      for (var i = options.colors.length; i < maxGroups; i++) {
        options.colors.push("hsl(" + ((360 / maxGroups) * i) + ", 60%, 60%)");
      }
    }

    // Pie charts
    var radius = 100;
    var pie_layout = d3.layout.pie()
                              .sort(null);

    var svg = d3.select(options.selector)
      .append("svg:svg")
      .attr("class", "pie chart")
      .attr('preserveAspectRatio', 'xMidYMid')
      .attr('viewBox', '0 0 ' + chart_attr.width + ' ' + chart_attr.height);

    var defs = svg.append('defs');

    var chart_size = d3.min([options.width, options.height]);

    var arc = d3.svg.arc()
                    .innerRadius(function(d, i, j) { return j * (chart_size / 2 / options.labels.x.length); })
                    .outerRadius(function(d, i, j) { return (j+1) * (chart_size / 2 / options.labels.x.length); });

    var chart = svg.append("svg:g")
                   .attr('transform', 'translate(' + options.margin.left + ", "
                         + options.margin.top + ")");

    var pies = chart.append('g')
                    .attr('class', 'pies')
                    .attr('transform', 'translate(' + options.width / 2 + ', ' + options.height / 2 + ')');

    pies.selectAll('g.pie')
        .data(options.labels.x)
        .enter().append('svg:g')
          .attr('class', function(d,i) { return 'group pie pie-' + i; })
          .selectAll('path.slice')
          .data(function(d, i) {
            return pie_layout(
              options.data.groups.map(function(e) {
                return e.series[i];
              })
            );
          })
          .enter().append('path')
            .attr('class', function(d, i) { return 'slice slice-' + i; })
            .attr('d', arc)
            .attr('data-group-index', function(d, i) { return i; })
            .style('fill', function(d, i) { return options.colors[i]});

    $(options.selector + " svg g.pies g.pie path.slice")
      .on('mouseenter', function(e) {
        chart.selectAll("g.pies g.pie path.slice")
          .style({
            "opacity":      "0.6",
          });

        chart.selectAll("g.pies g.pie path.slice.slice-" + $(this).data("group-index"))
          .style({
            "opacity":      "1.0",
          });
      })
      .on('mouseout', function(e) {
        chart.selectAll("g.pies g.pie path.slice")
          .style({
            "opacity":      "1.0",
          });
      });

    var labels = chart.append('svg:g')
                      .attr('class', 'series labels');

    // Labels for stacked pie chart
    if (options.labels.x.length > 1) {
      labels.selectAll('line.label')
        .data(options.labels.x)
        .enter().append("svg:line")
          .attr('x1', options.width / 2)
          .attr('y1', function(d, i) {
            return (i+0.5) * (chart_size / 2 / options.labels.x.length);
          })
          .attr('x2', options.width)
          .attr('y2', function(d, i) {
            return (i+0.5) * (chart_size / 2 / options.labels.x.length);
          })
          .attr('class', 'label')
          .attr('stroke', 'rgba(255, 255, 255, 0.7)');

      labels.selectAll('rect.label')
        .data(options.labels.x)
        .enter().append("svg:rect")
          .attr('fill', 'rgba(255, 255, 255, 0.7)')
          .attr('class', 'label')
          .attr('data-series-index', function(d,i) { return options.labels.x.length-1-i; })
          .attr('x', options.width - ((options.width - chart_size - 20) / 2))
          .attr('y', function(d, i) {
            return (i+0.5) * (chart_size / 2 / options.labels.x.length) - 30;
          })
          .attr('height', 30)
          .attr('width', (options.width - chart_size - 20) / 2);

      var label_width = ((options.width - chart_size - 20) / 2);

      labels.selectAll('text.label')
        .data(options.labels.x)
        .enter().append("svg:text")
          .attr('x', options.width - label_width/2)
          .attr('y', function(d, i) {
            return (options.labels.x.length-1-i+0.5) * (chart_size / 2 / options.labels.x.length) - 15;
          })
          .attr('text-anchor', 'middle')
          .attr('data-series-index', function(d,i) { return i; })
          .attr('class', 'label')
          .style('alignment-baseline', 'central')
          .style('dominant-baseline', 'central')
          .text(function(d,i) { return d; });

      $(options.selector + " svg g.series.labels .label")
        .on('mouseenter', function(e) {
          chart.selectAll("g.pies g.pie")
            .style({
              "opacity":      "0.6",
            });

          chart.selectAll("g.pies g.pie.pie-" + $(this).data("series-index"))
            .style({
              "opacity":      "",
            });
        })
        .on('mouseout', function(e) {
          chart.selectAll("g.pies g.pie")
            .style({
              "opacity":      "",
            });
        });
    }

    var group_labels = chart.append('svg:g')
                            .attr('class', 'group labels')
                            .attr('transform', 'translate(' + options.width / 2 + ', ' + options.height / 2 + ')');

    var groups = options.data.groups.map(function(group) {
      return group.name;
    });

    // Add group labels
    // Put them by default at locations radiating out from graph
    //   from the center of the arcs that correspond
    // Fit them inside the chart, do not allow them to overlap circle
    //   or pie chart legend or group legend (Initially empty)
    // If lines from center of arc to text go through circle or pie
    //   chart legend, put those labels inside group legend
    // add group legend, repeat process until fit
    // Add lines

    var group_arc = d3.svg.arc()
                          .innerRadius(chart_size / 2 + 100)
                          .outerRadius(chart_size / 2 + 100);

    var edge_arc = d3.svg.arc()
                         .innerRadius(chart_size / 2)
                         .outerRadius(chart_size / 2);

    // Assign default label locations around circle
    group_labels.selectAll('text.slice')
      .data(pie_layout(
        options.data.groups.map(function(e) {
          return e.series[e.series.length-1];
        })
      ))
      .enter().append('text')
        .attr('class', 'slice')
        .attr('x', function(d) {
          return group_arc.centroid(d)[0];
        })
        .attr('y', function(d) {
          return group_arc.centroid(d)[1];
        })
        .attr('dy', -5)
        .attr('dx', function(d) {
          if ((d.endAngle + d.startAngle)/2 > Math.PI) {
            return -5;
          }
          else {
            return 5;
          }
        })
        .attr('text-anchor', function(d) {
          if ((d.endAngle + d.startAngle)/2 > Math.PI) {
            return "end";
          }
          else {
            return "start";
          }
        })
        .text(function(d, i) {
          return options.data.groups[i].name;
        })
        .style('fill', "#ddd");

    // Move labels that are not in the graph
    group_labels.selectAll('text').each(function() {
      var old_y = d3.select(this).attr('y');
      if (old_y < -(chart_attr.height/2)+30) {
        old_y = -(chart_attr.height/2)+30;
      }
      if (old_y > (chart_attr.height/2)-30) {
        old_y = (chart_attr.height/2)-30;
      }
      d3.select(this).attr('y', old_y);
    });

    // Move labels that are inside pie legend
    if (options.labels.x.length > 1) {
      var pie_legend = {
        x1: 0,
        y1: -0.5 * (chart_size / 2 / options.labels.x.length),
        x2: chart_attr.width / 2,
        y2: -(options.labels.x.length-0.5) * (chart_size / 2 / options.labels.x.length) - 30
      };

      group_labels.selectAll('text').each(function() {
        var old_y = d3.select(this).attr('y');
        var old_x = d3.select(this).attr('x');
        if (old_x > pie_legend.x1 &&
            old_y > pie_legend.y2 && old_y < pie_legend.y1) {
          old_y = pie_legend.y1 + 30;
        }
        d3.select(this).attr('y', old_y);
      });
    }

    // Add lines
    group_labels.selectAll('line')
      .data(pie_layout(
        options.data.groups.map(function(e) {
          return e.series[e.series.length-1];
        })
      ))
      .enter().append('line')
        .attr('class', 'slice')
        .attr('x1', function(d) {
          return edge_arc.centroid(d)[0];
        })
        .attr('y1', function(d) {
          return edge_arc.centroid(d)[1];
        })
        .attr('x2', function(d,i) {
          return group_labels.select('text:nth-child('+(i+1)+')').attr('x');
        })
        .attr('y2', function(d,i) {
          return group_labels.select('text:nth-child('+(i+1)+')').attr('y');
        })
        .style('stroke', function(d, i) {
          return options.colors[i];
        });

    group_labels.selectAll('line.underline')
      .data(options.data.groups)
      .enter().append('line')
        .attr('class', 'slice underline')
        .attr('x1', function(d,i) {
          var label = group_labels.select('text:nth-child('+(i+1)+')');
          if (label.attr('text-anchor') === 'end') {
            return label.node().getBBox().x;
          }
          else {
            return label.node().getBBox().x + label.node().getBBox().width;
          }
        })
        .attr('y1', function(d,i) {
          return group_labels.select('text:nth-child('+(i+1)+')').attr('y');
        })
        .attr('x2', function(d,i) {
          return group_labels.select('text:nth-child('+(i+1)+')').attr('x');
        })
        .attr('y2', function(d,i) {
          return group_labels.select('text:nth-child('+(i+1)+')').attr('y');
        })
        .style('stroke', function(d, i) {
          return options.colors[i];
        });

    uuid += 1;
  };
}

/*globals initOccamVizCore*/
/*globals initOccamVizGraphs*/
/*globals initOccamVizGraphsBars*/
/*globals initOccamVizGraphsPie*/
var initOccamViz = function (context) {
  initOccamVizCore(context);

  // Add a similar line as above for each module that you have.  If you have a
  // module named "Awesome Module," it should live in the file
  // "src/occamViz.awesome-module.js" with a wrapper function named
  // "initAwesomeModule".
  initOccamVizGraphs(context);
  initOccamVizGraphsBars(context);
  initOccamVizGraphsPie(context);
  initOccamVizGraphsLines(context);

  return context.OccamViz;
};

if (typeof define === 'function' && define.amd) {
  // Expose OccamViz as an AMD module if it's loaded with RequireJS or
  // similar.
  define(function () {
    return initOccamViz({});
  });
} else {
  // Load OccamViz normally (creating a OccamViz global) if not using an AMD
  // loader.
  initOccamViz(this);
}

// Your library may have many modules.  How you organize the modules is up to
// you, but generally speaking it's best if each module addresses a specific
// concern.  No module should need to know about the implementation details of
// any other module.

// Note:  You must name this function something unique.  If you end up
// copy/pasting this file, the last function defined will clobber the previous
// one.
function initOccamVizModule (context) {
  'use strict';

  var OccamViz = context.OccamViz;

  // A library module can do two things to the OccamViz Object:  It can extend
  // the prototype to add more methods, and it can add static properties.  This
  // is useful if your library needs helper methods.

  // PRIVATE MODULE CONSTANTS
  var MODULE_CONSTANT = true;

  // PRIVATE MODULE METHODS
  //

  /**
   *  An example of a private method.  Feel free to remove this.
   */
  function modulePrivateMethod () {
    return;
  }

  // LIBRARY STATIC PROPERTIES
  //

  /**
   * An example of a static OccamViz property.  This particular static property
   * is also an instantiable Object.
   * @constructor
   */
  OccamViz.OccamVizHelper = function () {
    return this;
  };

  // LIBRARY PROTOTYPE EXTENSIONS
  //
  // A module can extend the prototype of the OccamViz Object.

  /**
   * An example of a prototype method.
   * @return {string}
   */
  OccamViz.prototype.alternateGetReadOnlyVar = function () {
    // Note that a module can access all of the OccamViz instance variables with
    // the `this` keyword.
    return this._readOnlyVar;
  };

  if (DEBUG) {
    // DEBUG CODE
    //
    // Each module can have its own debugging section.  They all get compiled
    // out of the binary.
  }
}

} (this));
