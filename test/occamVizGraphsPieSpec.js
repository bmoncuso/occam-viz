function hslToRgb(h, s, l){
  var r, g, b;

  if (s == 0) {
    r = g = b = l; // achromatic
  }
  else {
    function hue2rgb(p, q, t) {
      if(t < 0) t += 1;
      if(t > 1) t -= 1;
      if(t < 1/6) return p + (q - p) * 6 * t;
      if(t < 1/2) return q;
      if(t < 2/3) return p + (q - p) * (2/3 - t) * 6;
      return p;
    }

    var q = l < 0.5 ? l * (1 + s) : l + s - l * s;
    var p = 2 * l - q;
    r = hue2rgb(p, q, h + 1/3);
    g = hue2rgb(p, q, h);
    b = hue2rgb(p, q, h - 1/3);
  }

  return [Math.round(r * 255), Math.round(g * 255), Math.round(b * 255)];
}

function colorToHex(a) {
  if (a.substring(0, 3) === "rgb") {
    // parse "rgb(...)" colors
    var rgb = a.match(/^rgb\((\d+),\s*(\d+),\s*(\d+)\)$/);
    function hex(x) {
      return ("0" + parseInt(x).toString(16)).slice(-2);
    }
    return "#" + hex(rgb[1]) + hex(rgb[2]) + hex(rgb[3]);
  }
  else if (a.substring(0, 3) === "hsl") {
    // parse "hsl(...)" colors
    var hsl = a.match(/^hsl\((\d+),\s*(\d+),\s*(\d+)\)$/);
    var rgb = hsltoRgb(hsl[1], hsl[2], hsl[3]);
    function hex(x) {
      return ("0" + parseInt(x).toString(16)).slice(-2);
    }
    return "#" + hex(rgb[0]) + hex(rgb[1]) + hex(rgb[2]);
  }
  else if (a.substring(0, 1) === "#") {
    if (a.length === 4) {
      // expand
      a = "#" + a.substring(1,1) + "0"
              + a.substring(2,1) + "0"
              + a.substring(3,1) + "0";
    }

    // Cap to 7 digits
    a = a.substring(0, 7);
  }
  else {
    var colors = {
      "aliceblue":"#f0f8ff","antiquewhite":"#faebd7","aqua":"#00ffff","aquamarine":"#7fffd4","azure":"#f0ffff",
      "beige":"#f5f5dc","bisque":"#ffe4c4","black":"#000000","blanchedalmond":"#ffebcd","blue":"#0000ff","blueviolet":"#8a2be2","brown":"#a52a2a","burlywood":"#deb887",
      "cadetblue":"#5f9ea0","chartreuse":"#7fff00","chocolate":"#d2691e","coral":"#ff7f50","cornflowerblue":"#6495ed","cornsilk":"#fff8dc","crimson":"#dc143c","cyan":"#00ffff",
      "darkblue":"#00008b","darkcyan":"#008b8b","darkgoldenrod":"#b8860b","darkgray":"#a9a9a9","darkgreen":"#006400","darkkhaki":"#bdb76b","darkmagenta":"#8b008b","darkolivegreen":"#556b2f",
      "darkorange":"#ff8c00","darkorchid":"#9932cc","darkred":"#8b0000","darksalmon":"#e9967a","darkseagreen":"#8fbc8f","darkslateblue":"#483d8b","darkslategray":"#2f4f4f","darkturquoise":"#00ced1",
      "darkviolet":"#9400d3","deeppink":"#ff1493","deepskyblue":"#00bfff","dimgray":"#696969","dodgerblue":"#1e90ff",
      "firebrick":"#b22222","floralwhite":"#fffaf0","forestgreen":"#228b22","fuchsia":"#ff00ff",
      "gainsboro":"#dcdcdc","ghostwhite":"#f8f8ff","gold":"#ffd700","goldenrod":"#daa520","gray":"#808080","green":"#008000","greenyellow":"#adff2f",
      "honeydew":"#f0fff0","hotpink":"#ff69b4",
      "indianred ":"#cd5c5c","indigo ":"#4b0082","ivory":"#fffff0","khaki":"#f0e68c",
      "lavender":"#e6e6fa","lavenderblush":"#fff0f5","lawngreen":"#7cfc00","lemonchiffon":"#fffacd","lightblue":"#add8e6","lightcoral":"#f08080","lightcyan":"#e0ffff","lightgoldenrodyellow":"#fafad2",
      "lightgrey":"#d3d3d3","lightgreen":"#90ee90","lightpink":"#ffb6c1","lightsalmon":"#ffa07a","lightseagreen":"#20b2aa","lightskyblue":"#87cefa","lightslategray":"#778899","lightsteelblue":"#b0c4de",
      "lightyellow":"#ffffe0","lime":"#00ff00","limegreen":"#32cd32","linen":"#faf0e6",
      "magenta":"#ff00ff","maroon":"#800000","mediumaquamarine":"#66cdaa","mediumblue":"#0000cd","mediumorchid":"#ba55d3","mediumpurple":"#9370d8","mediumseagreen":"#3cb371","mediumslateblue":"#7b68ee",
      "mediumspringgreen":"#00fa9a","mediumturquoise":"#48d1cc","mediumvioletred":"#c71585","midnightblue":"#191970","mintcream":"#f5fffa","mistyrose":"#ffe4e1","moccasin":"#ffe4b5",
      "navajowhite":"#ffdead","navy":"#000080",
      "oldlace":"#fdf5e6","olive":"#808000","olivedrab":"#6b8e23","orange":"#ffa500","orangered":"#ff4500","orchid":"#da70d6",
      "palegoldenrod":"#eee8aa","palegreen":"#98fb98","paleturquoise":"#afeeee","palevioletred":"#d87093","papayawhip":"#ffefd5","peachpuff":"#ffdab9","peru":"#cd853f","pink":"#ffc0cb","plum":"#dda0dd","powderblue":"#b0e0e6","purple":"#800080",
      "red":"#ff0000","rosybrown":"#bc8f8f","royalblue":"#4169e1",
      "saddlebrown":"#8b4513","salmon":"#fa8072","sandybrown":"#f4a460","seagreen":"#2e8b57","seashell":"#fff5ee","sienna":"#a0522d","silver":"#c0c0c0","skyblue":"#87ceeb","slateblue":"#6a5acd","slategray":"#708090","snow":"#fffafa","springgreen":"#00ff7f","steelblue":"#4682b4",
      "tan":"#d2b48c","teal":"#008080","thistle":"#d8bfd8","tomato":"#ff6347","turquoise":"#40e0d0",
      "violet":"#ee82ee",
      "wheat":"#f5deb3","white":"#ffffff","whitesmoke":"#f5f5f5",
      "yellow":"#ffff00","yellowgreen":"#9acd32"};

    if (typeof colors[a.toLowerCase()] != 'undefined') {
      a = colors[a.toLowerCase()];
    }
  }

  return a;
}

function compareColor(a, b) {
  return colorToHex(a) == colorToHex(b);
}

describe("occamViz.graphs.pie", function() {
  describe("pie", function() {
    it("should append an svg to the given selector", function() {
      setFixtures(sandbox({
        id: "chart"
      }));

      var occam = new OccamViz();

      occam.graphs.pie({
        selector: '#chart'
      });

      expect($('#chart svg')).toExist();
    });

    it("should add a box to the legend for each series in the data", function() {
      setFixtures(sandbox({
        id: "chart"
      }));

      var occam = new OccamViz();

      occam.graphs.pie({
        selector: '#chart',
        labels: {x: ["foo", "bar", "baz"]},
        data: {
          groups: [
            { series: [0, 1, 2] },
            { series: [3, 4, 5] },
            { series: [3, 6, 7] },
            { series: [3, 1, 5] },
            { series: [0, 4, 3] },
          ]
        }
      });

      expect($('#chart svg g.series.labels rect.label').length).toEqual(3);
    });

    it("should add a pie chart for each series", function() {
      setFixtures(sandbox({
        id: "chart"
      }));

      var occam = new OccamViz();

      occam.graphs.pie({
        selector: '#chart',
        data: {
          groups: [
            { series: [0, 1, 2] },
            { series: [3, 4, 5] },
            { series: [3, 6, 7] },
            { series: [3, 1, 5] },
            { series: [0, 4, 3] },
          ]
        }
      });

      expect($('#chart svg g.pies g.group.pie').length).toEqual(3);
    });

    it("should add a pie wedge for each corresponding group value", function() {
      setFixtures(sandbox({
        id: "chart"
      }));

      var occam = new OccamViz();

      occam.graphs.pie({
        selector: '#chart',
        labels: {
          y: "blarg"
        },
        data: {
          groups: [
            { series: [0, 1, 2] },
            { series: [3, 4, 5] },
            { series: [3, 6, 7] },
            { series: [3, 1, 5] },
            { series: [0, 4, 3] },
          ]
        }
      });

      expect($('#chart svg g.pies g.pie:first-child path.slice').length).toEqual(5);
    });

    it("should abide by the x axis labels given", function() {
      setFixtures(sandbox({
        id: "chart"
      }));

      var occam = new OccamViz();

      occam.graphs.pie({
        selector: '#chart',
        labels: {
          x: ["1", "2", "z"],
          y: "blarg"
        },
        data: {
          groups: [
            { name: "a", series: [0, 1, 2] },
            { name: "b", series: [3, 4, 5] },
            { name: "c", series: [3, 6, 7] },
            { name: "d", series: [3, 1, 5] },
            { name: "e", series: [0, 4, 3] },
          ]
        }
      });

      var series_text = $('#chart svg g.series.labels text.label');
      series_text = $.map(series_text, function(e,i) {
        return e.textContent;
      });

      expect(series_text).toEqual(["1", "2", "z"]);
    });

    it("should put group names in the legend as given", function() {
      setFixtures(sandbox({
        id: "chart"
      }));

      var occam = new OccamViz();

      occam.graphs.pie({
        selector: '#chart',
        labels: {
          x: ["1", "2", "z"],
          y: "blarg"
        },
        data: {
          groups: [
            { name: "a", series: [0, 1, 2] },
            { name: "z", series: [3, 4, 5] },
            { name: "c", series: [3, 6, 7] },
            { name: "d", series: [3, 1, 5] },
            { name: "e", series: [0, 4, 3] },
          ]
        }
      });

      var axes_text = $('#chart svg g.group.labels text.slice');
      axes_text = $.map(axes_text, function(e) {
        return e.textContent;
      });

      expect(axes_text).toEqual(["a", "z", "c", "d", "e"]);
    });

    it("should match the color of the legend to the wedge it represents", function() {
      setFixtures(sandbox({
        id: "chart"
      }));

      var occam = new OccamViz();

      occam.graphs.pie({
        selector: '#chart',
        labels: {
          x: ["1", "2", "z"],
          y: "blarg"
        },
        data: {
          groups: [
            { name: "a", series: [0, 1, 2] },
            { name: "z", series: [3, 4, 5] },
            { name: "c", series: [3, 6, 7] },
            { name: "d", series: [3, 1, 5] },
            { name: "e", series: [0, 4, 3] },
          ]
        }
      });

      var legend_lines = $('#chart svg g.group.labels line.slice');
      legend_lines = $.map(legend_lines.slice(0, 5), function(e) {
        return e.style.stroke;
      });

      var pie_slices = $('#chart svg g.pies g.pie').first().find('path.slice');
      pie_slices = $.map(pie_slices, function(e) {
        return e.style.fill;
      });

      expect(legend_lines).toEqual(pie_slices);
    });

    it("should give unique colors to each group when no colors given", function() {
      setFixtures(sandbox({
        id: "chart"
      }));

      var occam = new OccamViz();

      occam.graphs.pie({
        selector: '#chart',
        labels: {
          x: ["1", "2", "z"],
          y: "blarg"
        },
        data: {
          groups: [
            { name: "a", series: [0, 1, 2] },
            { name: "z", series: [3, 4, 5] },
            { name: "c", series: [3, 6, 7] },
            { name: "d", series: [3, 1, 5] },
            { name: "e", series: [0, 4, 3] },
          ]
        }
      });

      var pie_slices = $('#chart svg g.pies g.pie').first().find('path.slice');
      pie_slices = $.map(pie_slices, function(e) {
        return colorToHex(e.style.fill);
      });

      var unique_colors = pie_slices.filter(function(e, pos) {
        return pie_slices.indexOf(e) == pos;
      });

      expect(unique_colors.length).toEqual(5);
    });

    it("should use the colors given", function() {
      setFixtures(sandbox({
        id: "chart"
      }));

      var occam = new OccamViz();

      var colors = ["red", "rgb(50, 30, 20)", "#0000ff", "#ff00ff", "#00ffff"];

      occam.graphs.pie({
        selector: '#chart',
        labels: {
          x: ["1", "2", "z"],
          y: "blarg"
        },
        colors: colors,
        data: {
          groups: [
            { name: "a", series: [0, 1, 2] },
            { name: "z", series: [3, 4, 5] },
            { name: "c", series: [3, 6, 7] },
            { name: "d", series: [3, 1, 5] },
            { name: "e", series: [0, 4, 3] },
          ]
        }
      });

      var pie_slices = $('#chart svg g.pies g.pie').first().find('path.slice');
      pie_slices = $.map(pie_slices, function(e) {
        return colorToHex(e.style.fill);
      });

      expect(pie_slices).toEqual(colors.map(colorToHex));
    });

    it("should dim other slices in all pies when a slice is hovered", function() {
      setFixtures(sandbox({
        id: "chart"
      }));

      var occam = new OccamViz();

      occam.graphs.pie({
        selector: '#chart',
        labels: {
          x: ["1", "2", "z"],
          y: "blarg"
        },
        data: {
          groups: [
            { name: "a", series: [0, 1, 2] },
            { name: "z", series: [3, 4, 5] },
            { name: "c", series: [3, 6, 7] },
            { name: "d", series: [3, 1, 5] },
            { name: "e", series: [0, 4, 3] },
          ]
        }
      });

      var pie_slice = $('#chart svg g.pies').first().find('path.slice').first();

      pie_slice.trigger("mouseenter");

      var slice_class = "slice-" + pie_slice.data('group-index');
      var other_pie_slices = $('#chart svg g.pies g.pie path.slice').filter(function(e, pos) {
        return $(this).attr('class').indexOf(" " + slice_class) == -1;
      });

      other_pie_slices = $.map(other_pie_slices, function(e,i) {
        return e.style.opacity;
      });

      var opacities = other_pie_slices.filter(function(e, pos) {
        return e == "0.6";
      });

      expect(opacities.length).toBe(12);
    });

    it("should return the graph to normal when a slice is not hovered", function() {
      setFixtures(sandbox({
        id: "chart"
      }));

      var occam = new OccamViz();

      occam.graphs.pie({
        selector: '#chart',
        labels: {
          x: ["1", "2", "z"],
          y: "blarg"
        },
        data: {
          groups: [
            { name: "a", series: [0, 1, 2] },
            { name: "z", series: [3, 4, 5] },
            { name: "c", series: [3, 6, 7] },
            { name: "d", series: [3, 1, 5] },
            { name: "e", series: [0, 4, 3] },
          ]
        }
      });

      var pie_slice = $('#chart svg g.pies').first().find('path.slice').first();

      pie_slice.trigger("mouseenter");
      pie_slice.trigger("mouseout");

      var slice_class = "slice-" + pie_slice.data('group-index');
      var pie_slices = $('#chart svg g.pies g.pie path.slice');

      var opacities = $.map(pie_slices, function(e, pos) {
        return e.style.opacity;
      });

      opacities = opacities.filter(function(e, pos) {
        return e != "0.6";
      });

      expect(opacities.length).toBe(15);
    });
  });
});
