# OCCAM Visualization Toolbox

This project is a set of javascript libraries that will visualize simulation
results on the client-side through OCCAM's JSON emitting front-end.

## Project Layout

```
:::text

dist               - Final distributable forms (compiled by a tool)
\- occamViz.js     - concatenation of our source code into one file
\- occamViz.min.js - minimal version built to minimize file size

src                - application code

vendor             - external js libraries we need
\- jquery.min.js   - jQuery (main abstraction of DOM/HTML)
\- d3.min.js       - d3 (graphing and abstraction of SVG)

doc                - API documentation

test               - Tests for our functionality
```

## Usage

Pull the distributable from the dist directory. Look at our documentation
for more information in the doc directory.

## Development

### Environment setup

Locally install node-js and then run the following command to install
our project dependencies.

```
:::text
npm install
```

### Quick Tasks

```
:::text
grunt jsdoc             - generate documentation in /doc
grunt build             - builds file js in /dist
grunt jasmine           - runs our unit tests in /test
grunt jshint            - checks for errors and code style for all code in /src
```

### Compiling

The final 'compiled' javascript file is made by concatenating several smaller
modular files in the `src` directory in this fashion:

```
:::text
src/occamViz.intro.js               - intro shim
src/occamViz.const.js               - constants
src/occamViz.core.js                - core functions
src/occamViz.*.js                   - rest of the modules
src/submodule/occamViz.*.js         - submodules as desired
src/occamViz.init.js                - library initialization
src/occamViz.outro.js               - outro shim
```

This becomes `dist/occamViz.js`.

The concatenated source is then minified by a processor that takes out redundant
syntax and whitespace. The resulting file is barely readable. This becomes
`dist/occamViz.min.js`. Use the former when debugging and the later in
production.

When adding a core feature, add a new module. (Handling certain inputs from
OCCAM?) When fleshing out a module with features, add a submodule. Be
modular by default!

To compile the javascript use the following command:

```
:::text
grunt build
```

To only compile the javascript when both tests and lint testing pass use simply:

```
:::text
grunt
```

### Documentation

When writing functions, document their parameters and such using `jsdoc`
syntax using [this guide](http://usejsdoc.org/tags-param.html).

To build the documentation specifically, use this command:

```
:::text
grunt jsdoc
```

### Lint Testing

Lint testing will look for particular errors or convention warnings in the code.
The code should abide by these warnings. To perform this check specifically, use
the following command:

```
:::text
grunt jshint
```

### Testing

When writing functions, write their tests (generally before you write the code
for that function) using [jasmine](http://jasmine.github.io/) with
[this guide](http://jasmine.github.io/2.0/introduction.html).

You can run them in a browser or the command line.
For the browser, you may need to run a local webserver if you are developing on
a headless server:

```
:::text
# Install:
npm install webserver

# Run:
node node_modules/webserver/webserver.js
```

You may then open the page:

```
:::text
localhost:8003/test/jasmine.occamViz.html
```

Otherwise, just open the `jasmine.occamViz.html` file and it will automatically
run the test suite. You may want to run it within several web browsers just in
case.

Or, simply run the tests in the command line via:

```
:::text
grunt jasmine
```

When dealing with AJAX requests, use
[mockjax](https://github.com/appendto/jquery-mockjax).
